import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {Router} from '@angular/router';
import { AngularFireList, AngularFireDatabase} from '@angular/fire/database';
import { AuthService} from '../auth.service';
import { ItemsService } from '../items.service';
import { noop } from 'rxjs';
import { ChangeDetectorStatus } from '@angular/core/src/change_detection/constants';


@Component({
  selector: 'item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {
  @Input() data:any;
  @Output() myButtonClick=new EventEmitter<any>();
  

  name;
  id;
  status;
  price;
  tempText;
  key;
  instock:boolean;

  showButton = false;
  showEditField=false;

  checked:boolean;
 

  delete(){
    this.itemsService.delete(this.key);
  }
 changeStock(){
  console.log(!this.checked);

  this.itemsService.changeStock(this.key,!this.checked);

 }
 // showEdit(){
//    this.showEditField= true;
 //   this.tempText=this.text;
 //  
 // }

 // cancel(){
 //   this.text=this.tempText;
 //   this.showEditField=false;
 // }


  over(){
    this.showButton = true;
  }
  notOver(){
    this.showButton = false;
  }
  constructor(private authService:AuthService, private itemsService:ItemsService, private db:AngularFireDatabase) { }

  ngOnInit() {
    this.name = this.data.name;
    this.price = this.data.price;
    //this.instock = this.data.instock;
    this.key = this.data.$key
    this.checked = this.data.instock;


  }

}