import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { AuthService} from '../auth.service';

@Component({
  selector: 'nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export  class NavComponent implements OnInit {
  errM;

  toItems(){
    this.router.navigate(['/items']);
  }
  toLogin(){
    this.router.navigate(['/login']);
  }
  toRegister(){
    this.router.navigate(['/register']);
  }
  toLogout(){
    this.authService.logout().
      then(value=>{this.router.navigate(['/login'])
    })
    .catch(err=>{
      this.errM = err;
      console.log("blalalalalal");
    })
  }
 
 

  constructor(public authService:AuthService,public router:Router) { }

  ngOnInit() {
  }

}
