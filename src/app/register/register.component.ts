import { Component, OnInit } from '@angular/core';
import {MatInputModule} from '@angular/material/input';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { AuthService} from '../auth.service';
import {Router} from '@angular/router';



@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})


export class RegisterComponent implements OnInit {

  email = new FormControl('', [Validators.required, Validators.email]);
  email1: string;
  password: string;
  name: string;
  errM;

  getErrorMessage() {
    return this.email.hasError('required') ? 'You must enter a value' :
        this.email.hasError('email') ? 'Not a valid email' :
            '';
  }

  constructor(private authService:AuthService, private router:Router) { }

  ngOnInit() {
  }

  signUp(){
   return this.authService.signUp(this.email1,this.password)
      .then(value =>{
        this.authService.updateProfile(value.user,this.name);
        this.authService.addUser(value.user,this.name);
      })

        .then(value=>{
          this.router.navigate(['/items']);
        })
          .catch(err=>{
            this.errM = err;
            console.log(err);
            console.log(this.authService.user);
          })
    
  
  }

}