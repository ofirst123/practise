import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ItemsComponent } from './items/items.component';
import { ItemComponent } from './item/item.component';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { NavComponent } from './nav/nav.component';

import {Routes, RouterModule} from '@angular/router';

import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatFormFieldModule} from '@angular/material/form-field';
import { ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatListModule} from '@angular/material/list';
import { FormsModule} from '@angular/forms';
import { MainComponent } from './main/main.component';
import {MatCheckboxModule} from '@angular/material/checkbox';

import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';

import { environment} from '../environments/environment';
import { RegisterComponent } from './register/register.component';
import { GreetingsComponent } from './greetings/greetings.component';

@NgModule({
  declarations: [
    AppComponent,
    ItemsComponent,
    ItemComponent,
    LoginComponent,
    NotFoundComponent,
    NavComponent,
    MainComponent,
    RegisterComponent,
    GreetingsComponent
  ],
  imports: [
    BrowserModule,
    MatCardModule,
    FormsModule,
   MatInputModule,
   MatFormFieldModule,
   BrowserAnimationsModule,
   ReactiveFormsModule,
   MatButtonModule,
   MatListModule, 
   AngularFireModule.initializeApp(environment.firebase),
   AngularFireDatabaseModule,
   AngularFireAuthModule,
   MatCheckboxModule,
    RouterModule.forRoot([
      {path:'', component:LoginComponent},
      {path:'items', component:ItemsComponent},
      {path:'register', component:RegisterComponent},
      {path:'login', component:LoginComponent},
      {path:'**', component:NotFoundComponent}
 
    ])
 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
