import { Injectable } from '@angular/core';
import { AuthService} from './auth.service';
import { AngularFireList, AngularFireDatabase} from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class ItemsService {

  addItem(newItem,newPrice,checked){
    this.authService.user.subscribe(user=>{
      this.db.list('/users/'+user.uid+'/items/').push({'name':newItem,'price':newPrice,'instock':checked});
    })
  }
  delete(key){
    this.authService.user.subscribe(user=>{
      this.db.list('/users/'+user.uid+'/items/').remove(key);
    })
  }
  changeStock(key,checked:boolean){
    this.authService.user.subscribe(user=>{
      this.db.list('/users/'+user.uid+'/items/').update(key,{'instock':checked});
    })

  }

  constructor(private authService:AuthService, private db:AngularFireDatabase) { }
}
