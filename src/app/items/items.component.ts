import { Component, OnInit } from '@angular/core';
import { AngularFireList, AngularFireDatabase} from '@angular/fire/database';
import { AuthService} from '../auth.service';

import { ItemsService } from '../items.service';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {

  items = [];

  name;
  newItem;
  newPrice;
  checked:boolean;

  showText($event){
    this.name=$event;
    
  }


  constructor(private authService:AuthService, private itemsService:ItemsService, private db:AngularFireDatabase) { }

  addItem(){
    this.itemsService.addItem(this.newItem,this.newPrice,this.checked);
    this.newItem='';
    this.newPrice='';
    this.checked= false;

  }

  ngOnInit() {
    this.authService.user.subscribe(user=> {
      this.db.list('/users/'+user.uid+'/items/').snapshotChanges().subscribe(
        items=>{
          this.items = [];
          items.forEach(
            item =>{
              let y = item.payload.toJSON();
              y["$key"] = item.key;
              this.items.push(y);
            }
          )
        }
      )

    })


  }

}
